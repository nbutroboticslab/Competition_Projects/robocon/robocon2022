#ifndef POSTURE_TASK_H
#define POSTURE_TASK_H

#include "gait_param.h"

void Posture_Task(void *argument);
void Change_NowState(Robohorse_State target);

#endif /*POSTURE_TASK_H*/
