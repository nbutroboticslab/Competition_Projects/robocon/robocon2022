#ifndef BUZZER_FREQ_H
#define BUZZER_FREQ_H

// 频率表：
// 参考 https://blog.csdn.net/leonliu070602/article/details/76254153?spm=1001.2101.3001.6650.17&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-17.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-17.pc_relevant_default&utm_relevant_index=19
// 我没时间写了，等一个有缘人 (>_<)
//      note       freq(Hz*10)
#define NOTE_A2   (275U) //1钢琴第一个键
#define NOTE_A2S  (291U)
#define NOTE_B2   (308U)

#define NOTE_C1   (327U) //C0
#define NOTE_C1S  (346U)
#define NOTE_D1   (367U)
#define NOTE_D1S  (389U)
#define NOTE_E1   (412U)
#define NOTE_F1   (437U)
#define NOTE_F1S  (462U)
#define NOTE_G1   (490U)
#define NOTE_G1S  (519U)
#define NOTE_A1   (550U)
#define NOTE_A1S  (583U)
#define NOTE_B1   (617U)

#define NOTE_C    (654U)//C1
#define NOTE_CS   (693U)
#define NOTE_D    (734U)
#define NOTE_DS   (778U)
#define NOTE_E    (824U)
#define NOTE_F    (873U)
#define NOTE_FS   (925U)
#define NOTE_G    (980U)
#define NOTE_GS   (1038U)
#define NOTE_A    (1100U)
#define NOTE_AS   (1165U)
#define NOTE_B    (1235U)

#define NOTE_c    (1308U)//C2
#define NOTE_cS   (1386U)
#define NOTE_d    (1468U)
#define NOTE_dS   (1556U)
#define NOTE_e    (1648U)
#define NOTE_f    (1746U)
#define NOTE_fS   (1850U)
#define NOTE_g    (1960U)
#define NOTE_gS   (2077U)
#define NOTE_a    (2200U)
#define NOTE_aS   (2331U)
#define NOTE_b    (2469U)

#define NOTE_c1   (2616U)//C3 中央C
#define NOTE_c1S  (2772U)
#define NOTE_d1   (2937U)
#define NOTE_d1S  (3111U)
#define NOTE_e1   (3296U)
#define NOTE_f1   (3492U)
#define NOTE_f1S  (3700U)
#define NOTE_g1   (3920U)
#define NOTE_g1S  (4153U)
#define NOTE_a1   (4400U)
#define NOTE_a1S  (4662U)
#define NOTE_b1   (4939U)

#define Z0   (0U)


#endif // BUZZER_FREQ_H
