# robocon2022

## 目录
    ROBOCON2022
        ├─README.md
        ├─img
        |  ├─2022视频
        ├─doc
        |  ├─2023规则
        |  ├─2023会议记录
        |  ├─2022规则
        |  ├─2022程序任务
        |  ├─2022会议记录
        |  └─Manuals  ---相关教程
        |       └─image
        ├─bsp_for_RMboardA
        |        └─bsp_for_RMboardA(1)
        |                   └─bsp_for_RMboardA
        |                            └─test_proj
        |                                  └─robohorse2022
        ├─MDK-ARM   ---机器马代码
        ├─3D_build
        |    ├─外校的机械图纸
        |    ├─2023机械设计
        |    └─2022机械结构
        ├─robohorse2022.uvprojx - 快捷方式
        └─.git
