# TASK2 CAN总线驱动M2006/M3508无刷电机

## 任务内容：
1. 了解CAN总线协议的硬件驱动层及协议层，**理解CAN总线的报文组成**。
2. 使用HAL库配合CubeMX完成CAN总线的配置
3. 熟悉M2006/M3508的使用方法及报文发送，并驱动M2006/M3508电机，使它5秒正转5秒反转（**可以不用PID，但是电流不要太大，500mA左右就够了，注意安全，注意安全，注意安全！**）

## 参考资料
Robomaster产品,可以找到M2006/M3508和RM开发板(我们一般使用a型)的资料<https://www.robomaster.com/zh-CN/products/components/general?djifrom=nav>
